from django.db import models

class Users(models.Model):
    User_id = models.BigAutoField(primary_key=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone = models.IntegerField()
    address = models.CharField(max_length=100)

    def __str__(self):
        return self.first_name


# class AllExpenses(models.Model):

