from django.db import models
from django.db.models import fields
from rest_framework import serializers
from .models import Users



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['User_id','first_name','last_name','email']
        